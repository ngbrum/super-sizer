import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-setup',
  template: `
<div class="holder" fxLayout="column" fxLayoutAlign="start center">
  <h1>SIZE STORM</h1>
  <mat-card>
    <mat-form-field>
      <input matInput placeholder="Enter your name:" [(ngModel)]="user.name">
    </mat-form-field>
    <button mat-raised-button color="primary" (click)="save()">Save</button>
  </mat-card>
</div>
  `,
  styles: [
    `
      h1 {
        font-size: 60px;
        font-weight: 200;
      }
      .holder {
        height: 100vh;
        width: 100vw;
        padding-top: 10vh;
      }

      button {
        width: 100%;
      }

      mat-card {
        width: 40vw;
        max-width: 400px;
        min-width: 200px;
      }

      mat-form-field {
        width: 100%;
      }
    `
  ]
})
export class SetupComponent implements OnInit {
  user;
  constructor(private router: Router) {}

  ngOnInit() {
    const storedUser = localStorage.getItem('user');
    this.user = storedUser ? JSON.parse(storedUser) : { name: '' };
  }

  save() {
    localStorage.setItem('user', JSON.stringify(this.user));
    this.router.navigateByUrl('/play');
  }
}
