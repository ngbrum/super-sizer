import { Injectable } from '@angular/core';
import { GameModel, Tile, LevelBuilder } from './play.models';

@Injectable({
  providedIn: 'root'
})
export class PlayService {
  constructor() {}

  initGame(): GameModel {
    return {
      stage: 'play',
      time: 5,
      startingTime: 5,
      score: 0,
      level: 1,
      lives: 3,
      collectedTiles: []
    };
  }

  // generate a 2d array representing the grid
  initLevel(level: LevelBuilder): Tile[] {
    // First, how many numbers do we need?
    const amountOfNumbers = Math.pow(level.gridSize, 2);

    // initialise an empty array that will be the final level
    const generatedLevel: Tile[] = [];

    // lets fill up the array with numbers!
    // We'll create incrementally bigger number, then shuffle.
    let minNumber = level.minNumber;
    for (let x = 0; x < amountOfNumbers; x++) {
      // Don't start with a very big number
      const maxNumber = level.maxNumber - amountOfNumbers + x;
      const number = Math.ceil(
        Math.random() * (maxNumber - minNumber) + minNumber
      );
      minNumber = number;
      const tile: Tile = {
        display: number.toString(),
        answer: number,
        touched: false,
        correct: false,
        sequence: x + 1
      };
      generatedLevel.push(tile);
    }
    // shuffle it up
    let shuffledLevel = this.shuffleArray(generatedLevel);

    if (level.addition) {
      this.convertToAddition(shuffledLevel, level);
      shuffledLevel = this.shuffleArray(generatedLevel);
    }

    if (level.subtraction) {
      this.convertToSubtraction(shuffledLevel, level);
      shuffledLevel = this.shuffleArray(generatedLevel);
    }

    return shuffledLevel;
  }

  private shuffleArray(generatedLevel) {
    return generatedLevel.sort(() => Math.random() - 0.5);
  }

  private convertToAddition(shuffledLevel, level) {
    // convert to addition questions
    const tilesToConvert = shuffledLevel.slice(0, level.addition);
    tilesToConvert.forEach(tile => {
      const smallerNumber = Math.floor(Math.random() * tile.answer);
      const secondNumber = tile.answer - smallerNumber;
      tile.display = `${smallerNumber} + ${secondNumber}`;
    });
  }

  private convertToSubtraction(shuffledLevel, level) {
    const biggestDiffernce = 5;
    const tilesToConvert = shuffledLevel
      .filter(tile => tile.answer.toString() === tile.display)
      .slice(0, level.subtraction);
    tilesToConvert.forEach(tile => {
      const minVal =
        tile.answer > biggestDiffernce ? tile.answer - biggestDiffernce : 0;
      const smallerNumber = Math.floor(
        Math.random() * (tile.answer - minVal) + minVal
      );
      const secondNumber = tile.answer + smallerNumber;
      tile.display = `${secondNumber} - ${smallerNumber}`;
    });
  }
}
