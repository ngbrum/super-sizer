import { Component, OnInit } from '@angular/core';
import { levelConfig } from '../levelBuilder';
import { PlayService } from '../play.service';
import { GameModel, Tile } from '../play.models';
import { timer } from 'rxjs';
import { tap, filter } from 'rxjs/operators';

@Component({
  selector: 'app-play',
  template: `
<div *ngIf="!game; else gamePlay" class="start-holder" fxLayout fxLayoutAlign="center center">
  <button mat-raised-button color="accent" (click)="startGame()" class="start-button">Start</button>
</div>
<ng-template #gamePlay>
  <ng-container [ngSwitch]="game?.stage">
    <div *ngSwitchCase="'play'" class="play-holder" fxLayout fxLayout="column">
      <mat-toolbar fxLayout>
        <span>Score: {{ game.score }} </span>
        <span fxFlex></span>
        <span>Lives: {{ game.lives }}</span>
      </mat-toolbar>
      <mat-grid-list [cols]="levels[game.level - 1].gridSize" rowHeight="fit" fxFlex gutterSize="0px">
        <mat-grid-tile class="grid-item" [ngClass]="{'correct': tile.correct}"
                       *ngFor="let tile of level" (click)="tilePressed(tile)">
                       <h1>{{ tile.display }}</h1>
                       </mat-grid-tile>
      </mat-grid-list>

      <div class="timer" fxLayout="column">
        <span [fxFlex]="((game.startingTime - game.time) / game.startingTime) * 100 + '%'"></span>
        <div fxFlex></div>
      </div>

    </div>
    <div *ngSwitchCase="'result'" fxLayout="column" fxLayoutAlign="center center" class="gameover-holder">
      <h1>Game Over!</h1>
      <mat-card>
        <h2>Score: {{ game.score }}</h2>
        <button mat-raised-button color="accent" (click)="startGame()">Play Again</button>
      </mat-card>
    </div>
  </ng-container>
</ng-template>
  `,
  styles: [
    `
      .play-holder {
        height: 100vh;
      }
      .start-button,
      .gameover-holder,
      .card-holder,
      .start-holder {
        height: 100vh;
        width: 100%;
      }

      .start-button {
        font-size: 20vh;
      }

      mat-grid-tile.correct {
        background-color: #87ab87;
        color: white;
      }

      mat-grid-tile {
        cursor: pointer;
        border: 2px solid black;
        box-sizing: border-box;
      }

      mat-toolbar {
        width: 100%;
        height: 64px !important;
      }

      mat-grid-list {
        z-index: 1;
      }

      .timer {
        position: fixed;
        top: 64px;
        bottom: 0;
        left: 0;
        right: 0;
        z-index: 0;
      }

      .timer div {
        background-color: #eaaadb;
        width: 100%;
      }

      .timer span {
        transition: all 0.1s linear;
      }
    `
  ]
})
export class PlayComponent implements OnInit {
  game: GameModel;
  level: Tile[];
  levels = levelConfig;
  timer;
  constructor(private playService: PlayService) {}

  ngOnInit() {}

  startGame() {
    const level = this.levels[0];
    this.level = this.playService.initLevel(level);
    this.game = this.playService.initGame();
    this.startTimer();
  }

  tilePressed(tile: Tile) {
    if (tile.correct) {
      return;
    }
    tile.touched = true;

    const collectionLength = this.game.collectedTiles.length;
    let lastSequenceNumber = 0;
    if (this.game.collectedTiles.length) {
      lastSequenceNumber = this.game.collectedTiles[collectionLength - 1]
        .sequence;
    }

    if (tile.sequence === lastSequenceNumber + 1) {
      tile.correct = true;
      this.game.collectedTiles.push(tile);
      this.game.score =
        this.game.score + this.levels[this.game.level - 1].points;
    } else {
      this.reduceLife();
    }

    if (this.game.collectedTiles.length >= this.level.length) {
      this.nextLevel();
    }
  }
  reduceLife() {
    this.game.lives = this.game.lives - 1;
    this.game.time = this.game.startingTime;
    if (this.game.lives <= 0) {
      if (this.timer.unsubscribe) {
        this.timer.unsubscribe();
      }
      return (this.game.stage = 'result');
    }
  }

  nextLevel() {
    if (this.game.level < this.levels.length) {
      this.game.level = this.game.level + 1;
    }
    const level = this.levels[this.game.level];
    this.level = this.playService.initLevel(level);
    this.game.collectedTiles = [];
    this.game.time = level.time;
    this.game.startingTime = level.time;
  }

  startTimer() {
    this.timer = timer(0, 100)
      .pipe(
        tap(() => (this.game.time = this.game.time - 0.1)),
        filter(() => this.game.time < 0),
        tap(() => this.reduceLife())
      )
      .subscribe();
  }
}
