import { LevelBuilder } from './play.models';
export const levelConfig: LevelBuilder[] = [
  {
    gridSize: 2,
    time: 10,
    maxNumber: 10,
    minNumber: 1,
    points: 200
  },
  {
    gridSize: 2,
    time: 10,
    maxNumber: 20,
    minNumber: 1,
    points: 300
  },
  {
    gridSize: 2,
    time: 10,
    maxNumber: 10,
    minNumber: 1,
    points: 400,
    addition: 1
  },
  {
    gridSize: 2,
    time: 10,
    maxNumber: 20,
    minNumber: 1,
    points: 500,
    addition: 2
  },
  {
    gridSize: 2,
    time: 10,
    maxNumber: 10,
    minNumber: 1,
    points: 600,
    subtraction: 1
  },
  {
    gridSize: 2,
    time: 10,
    maxNumber: 10,
    minNumber: 1,
    points: 800,
    subtraction: 2
  },
  {
    gridSize: 2,
    time: 10,
    maxNumber: 10,
    minNumber: 1,
    points: 800,
    subtraction: 2,
    addition: 1
  },
  {
    gridSize: 2,
    time: 10,
    maxNumber: 10,
    minNumber: 1,
    points: 800,
    subtraction: 2,
    addition: 1
  },
  {
    gridSize: 2,
    time: 10,
    maxNumber: 10,
    minNumber: 1,
    points: 800,
    subtraction: 2,
    addition: 2
  }
];
