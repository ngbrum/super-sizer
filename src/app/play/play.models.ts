export interface GameModel {
  stage: 'play' | 'result' | 'waiting';
  startingTime: number;
  time: number;
  score: number;
  level: number;
  lives: number;
  collectedTiles: Tile[];
}

export interface Tile {
  display: string;
  answer: number;
  touched: boolean;
  correct: boolean;
  sequence: number;
}

export interface LevelBuilder {
  gridSize: number;
  time: number;
  maxNumber: number;
  minNumber: number;
  points: number;
  addition?: number;
  subtraction?: number;
  multiplication?: boolean;
  division?: boolean;
}
